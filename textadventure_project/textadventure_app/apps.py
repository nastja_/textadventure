from django.apps import AppConfig


class TextadventureAppConfig(AppConfig):
    name = 'textadventure_app'
