from django.db import models


class Station(models.Model):
    name = models.CharField(max_length=50, unique=True)
    title = models.CharField(max_length=200)
    text = models.TextField()
    required_inventar = models.CharField(max_length=200, blank=True)
    def __str__(self):
        return self.name


class Choice(models.Model):
    c_station = models.ForeignKey(to=Station, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    next_station = models.ManyToManyField(Station, related_name="incoming_set", blank=True)

    def __str__(self):
        return f"{self.text}: {self.next_station.all()}"


class Inventar(models.Model):
    name = models.CharField(max_length=50)
    choice = models.ForeignKey(to=Choice, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Spieler(models.Model):
    name = models.CharField(max_length=50, unique=True)
    station = models.ForeignKey(to=Station, on_delete=models.CASCADE)
    inventar = models.ManyToManyField(Inventar, blank=True)
    def __str__(self):
        return f"{self.name} {self.inventar.name}"



