"""textadventure_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views

app_name = "textadventure_app"

urlpatterns = [
    path('', views.index, name='index'),
    path('station/<str:station_name>_<str:spieler>', views.station, name='station'),
    path('save_inventar/<int:choice_id>_<int:spieler_id>', views.save_inventar, name='save_inventar'),
    path('choose_station/<int:choice_id>_<int:spieler_id>', views.choose_station, name='choose_station'),
    path('save_spieler', views.save_spieler, name='save_spieler'),
]
