from django.shortcuts import render, get_object_or_404, get_list_or_404
from . import models
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound


def index(request):
    spieler = models.Spieler.objects.all()
    context = {"spieler": spieler}
    return render(request, "textadventure_app/index.html", context)

def station(request, station_name, spieler):
    spieler = get_object_or_404(models.Spieler, name=spieler)
    station = get_object_or_404(models.Station, name=station_name) 
    spieler.station = station
    choices =  get_list_or_404(models.Choice, c_station__name=station_name)
    if station_name == "start":
        spieler.inventar.clear()
    spieler.station = station
    spieler.save()
    sentences = station.text.split(".")
    context = {"station": station, "spieler": spieler, "choices": choices, "sentences": sentences}
    return render(request, "textadventure_app/station.html", context)


def save_inventar(request, choice_id, spieler_id):
    inventar = models.Inventar.objects.get(choice=choice_id)
    spieler = models.Spieler.objects.get(id=spieler_id)
    spieler.inventar.add(inventar)
    inventar.save()
    spieler.save()
    choice = get_object_or_404(models.Choice,id=choice_id)
    for st in choice.next_station.all():
        name = st.name
    return HttpResponseRedirect(reverse('textadventure_app:station', args=[name, spieler.name,]))


def choose_station(request, choice_id, spieler_id):
    spieler = models.Spieler.objects.get(id=spieler_id)
    choice = get_object_or_404(models.Choice,id=choice_id)

    if choice.next_station.all():
        for st in choice.next_station.all():
            if st.required_inventar in spieler.inventar.values_list("name", flat=True):
                name=st.name
                break
            else:
                name = st.name
    else:
        spieler.delete()
        return HttpResponseRedirect(reverse('textadventure_app:index'))
        
    return HttpResponseRedirect(reverse('textadventure_app:station', args=[name, spieler.name,]))


def save_spieler(request):
    new_spieler = models.Spieler()
    new_spieler.name = request.POST.get("name")
    new_spieler.station = models.Station.objects.get(name="start")
    new_spieler.save()
    return HttpResponseRedirect(reverse('textadventure_app:station', args=[new_spieler.station, new_spieler.name,]))


